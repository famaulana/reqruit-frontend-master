require('dayjs/locale/id')

import Vue from 'vue';
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import updateLocale from "dayjs/plugin/updateLocale";


dayjs.extend(relativeTime);
dayjs.extend(updateLocale);
dayjs.locale('id')

Object.defineProperties(Vue.prototype, {
    $dayjs: {
        get() {
            return dayjs
        }
    }
});
