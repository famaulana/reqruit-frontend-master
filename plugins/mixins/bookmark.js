import Vue from "vue";

const BookmarkMixin = {
    install(Vue, options) {
        Vue.mixin({
            data() {
                return {
                    marked: false,
                    bookmark_id: null,
                };
            },
            methods: {
                bookmark() {
                    if (!this.authenticated) {
                        this.swalToast(
                            "error",
                            "Kamu harus login untuk bookmark"
                        );
                        return;
                    }

                    if (this.marked && this.bookmark_id !== null) {
                        this.$axios
                            .$delete(`account/bookmarks/${this.bookmark_id}`)
                            .then((res) => {
                                this.marked = false;
                                this.emitDelete();
                                this.swalToast(res.status, res.message);
                            })
                            .catch((err) => {
                                if (err.response.status === 404) {
                                    this.emitDelete();
                                }
                                this.axiosError(err);
                            });
                    } else {
                        this.$axios
                            .$post("account/bookmarks", {
                                job_id: this.job.id,
                            })
                            .then((res) => {
                                this.marked = true;
                                this.bookmark_id = res.data.id;
                                this.$nuxt.$emit("bookmark_added", {
                                    job_id: this.job.id,
                                    bookmark_id: this.bookmark_id,
                                });
                                const updatedUser = { ...this.$auth.user };
                                updatedUser.bookmarks = [
                                    ...this.user.bookmarks,
                                    {
                                        id: res.data.id,
                                        job_id: this.job.id,
                                    },
                                ];
                                this.$auth.setUser(updatedUser);
                                this.swalToast(res.status, res.message);
                            })
                            .catch((err) => {
                                this.axiosError(err);
                            });
                    }
                },
                emitDelete() {
                    const bookmarks = [...this.user.bookmarks];
                    const index = bookmarks.findIndex(
                        (bm) => bm.id === this.bookmark_id
                    );
                    if (index > -1) {
                        bookmarks.splice(index, 1);
                    }

                    const updatedUser = { ...this.$auth.user };
                    updatedUser.bookmarks = bookmarks;
                    this.$auth.setUser(updatedUser);

                    this.$nuxt.$emit("bookmark_deleted", {
                        job_id: this.job.id,
                        bookmark_id: this.bookmark_id,
                    });
                },
            },
        });
    },
};

Vue.use(BookmarkMixin);
