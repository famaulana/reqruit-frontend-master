export default ({ app }, inject) => {
    inject("job_card", {
        sameHeight({
            wrapper,
            row_mobile,
            row_tablet,
            row_notebook,
            row_desktop,
        }) {
            const wrapper_el = document.querySelector(wrapper);
            const screen_width = window.innerWidth;
            let item_per_row = row_mobile;
            if (screen_width >= 768 && screen_width < 992) {
                item_per_row = row_tablet;
            }
            if (screen_width >= 992 && screen_width < 1024) {
                item_per_row = row_notebook;
            }
            if (screen_width >= 1024) {
                item_per_row = row_desktop;
            }

            if (!wrapper_el || screen_width < 576 || item_per_row == 1) {
                return;
            }

            setTimeout(() => {
                const job_card_count = wrapper_el.querySelectorAll(
                    ".job-card:not(.h-updated)"
                ).length;
                let row_loop_count = 1;
                let loop_count = 1;
                const row_data = [];
                let info_height = 0;

                new Promise((resolve, reject) => {
                    wrapper_el
                        .querySelectorAll(".job-card:not(.h-updated)")
                        .forEach((el) => {
                            if (
                                info_height <
                                el.querySelector(".job-card__info").offsetHeight
                            ) {
                                info_height =
                                    el.querySelector(
                                        ".job-card__info"
                                    ).offsetHeight;
                            }

                            if (item_per_row != "") {
                                if (
                                    row_loop_count >= item_per_row ||
                                    loop_count == job_card_count
                                ) {
                                    row_data.push({ info_height });

                                    row_loop_count = 0;
                                    info_height = 0;
                                }

                                row_loop_count++;
                            }

                            loop_count++;
                        });

                    if (item_per_row == "") {
                        row_data.push({ info_height });
                    }

                    resolve(row_data);
                }).then((row_data) => {
                    loop_count = 0;

                    wrapper_el
                        .querySelectorAll(".job-card:not(.h-updated)")
                        .forEach((el) => {
                            let data_index = 0;
                            if (item_per_row != "") {
                                data_index = Math.ceil(
                                    (loop_count + 1) / item_per_row
                                );
                                if (data_index > 0) {
                                    data_index = data_index - 1;
                                }
                            }
                            const data = row_data[data_index];

                            if (data.info_height > 0) {
                                el.querySelector(
                                    ".job-card__info"
                                ).style.height = `${data.info_height}px`;
                                el.classList.add("h-updated");
                            }

                            loop_count++;
                        });
                });
            }, 500);
        },
    });
};
